/*******************
  Michael John O'Brien
  mobrie5
  Lab 2
  Lab Section: 005
  Alex Myers
*******************/

#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include "stdlib.h"
#include "string.h"
using namespace std;

//Enumeration to represent suit values for a deck Of Cards
enum Suit { SPADES=0, HEARTS=1, DIAMONDS=2, CLUBS=3 };

//Struct for a standard playing card
typedef struct Card {
  Suit suit;
  int value;
} Card;

//This function will get the suit's code( to print to the screen)
string get_suit_code(Card& c);
//This function will get the suit
string get_card_name(Card& c);
//This function wil return either true or false
bool suit_order(const Card& lhs, const Card& rhs);
//This is the make random function
int myrandom (int i) { return std::rand()%i;}


int main(int argc, char const *argv[])
{
  /*This is to seed the random generator */
  srand(unsigned (time(0)));

   Card deckOfCards[52];
   //use a nested for loop to initialize all the cards
   int m = 0;
   for(int i = 0; i < 4; i++)
   {
     int valueForSuits = 2;
     for(int j = 0; j < 13; j++)
     {
       if (m >= 0 && m <= 12)
       {
         deckOfCards[m].value = valueForSuits;
         valueForSuits = valueForSuits + 1;
         deckOfCards[m].suit = static_cast<Suit>(i);
       }

       else if (m >= 13 && m <= 25)
       {
         deckOfCards[m].value = valueForSuits;
         valueForSuits = valueForSuits + 1;
         deckOfCards[m].suit = static_cast<Suit>(i);
       }
       else if (m >= 26 && m <= 38)
       {
         deckOfCards[m].value = valueForSuits;
         valueForSuits = valueForSuits + 1;
         deckOfCards[m].suit = static_cast<Suit>(i);
       }
       else if (m >= 39 && m <= 51)
       {
         deckOfCards[m].value = valueForSuits;
         valueForSuits = valueForSuits + 1;
         deckOfCards[m].suit = static_cast<Suit>(i);

       }
          m = m + 1;
     }
   }


    //call random_shuffle to shuffle the entire deck
    random_shuffle(deckOfCards, (deckOfCards + 52), myrandom);

    //hand of cards (5 cards)
    Card handOfCards[5] = {deckOfCards[0], deckOfCards[1], deckOfCards[2], deckOfCards[3],
    deckOfCards[4]};


    //Call the sort function to sort the hand of cards.
    sort(handOfCards, (handOfCards + 5), suit_order);
    //use a nested for loop to print out the deck of cards
    for (int i = 0; i < 5; i++)
     {
        if (handOfCards[i].value >= 11 && handOfCards[i].value <= 14)
        {
          cout << right << setw(10) << get_card_name(handOfCards[i]) << " of " << get_suit_code(handOfCards[i]) << endl;
        }
        else
        {
          cout << right << setw(10) << handOfCards[i].value << " of " << get_suit_code(handOfCards[i]) << endl;
        }
     }
    return 0;
}


//This function is passed to the sort function and it returns
//either true or false.
bool suit_order(const Card& lhs, const Card& rhs)
{
  if (lhs.suit < rhs.suit)
  {
    return true;
  }
  else if (lhs.suit == rhs.suit)
  {
    if (lhs.value < rhs.value)
    {
      return true;
    }
  }
  else
  {
    return false;
  }
  return false;
}

//This function returns the unicode string
//based on the suit
string get_suit_code(Card& c)
{
  switch (c.suit)
  {
    case SPADES:    return "\u2660";
    case HEARTS:    return "\u2661";
    case DIAMONDS:  return "\u2662";
    case CLUBS:     return "\u2663";
    default:        return "";
  }
}

//This function gets the name (jack, queen, king, ace)
//based on the card's value being either
//11, 12, 13, or 14.
string get_card_name(Card& c)
{
  switch (c.value)
  {
    case 11:      return "Jack";
    case 12:      return "Queen";
    case 13:      return "King";
    case 14:      return "Ace";
    default:      return "";
  }
}
